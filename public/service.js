(function() {
    angular
        .module('BookApp')
        .service('dbService', dbService);

    dbService.$inject = ['$http', '$q'];

    function dbService($http, $q) {

        var vm = this;

        vm.bookList = list;
        vm.bookDetail = detail;

        function list(limit, offset) {
            var defer = $q.defer();
            var params = {limit: limit || 125, offset: offset || 0};
            $http.get("/api/books", {params: params})
                .then(function (results) {
                    defer.resolve(results.data);
                });
            return (defer.promise);
        }

        function detail(bookId) {
            var defer = $q.defer();
            $http.get("/api/bookinfo/" + bookId)
                .then(function (result) {
                    defer.resolve(result.data);
                }).catch(function (error) {
                defer.reject(error);
            });
            return (defer.promise);
        }
        
    }
})();

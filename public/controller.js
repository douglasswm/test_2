(function () {
    angular
        .module('BookApp')
        .controller('listCtrl', listCtrl)
        .controller('detailCtrl', detailCtrl);


    listCtrl.$inject = ['$state', 'dbService', '$stateParams'];

    function listCtrl($state, dbService, $stateParams) {

        var vm = this;
        vm.books = 1;
        vm.data = 1;
        vm.getDetails = function (bookId) {
            $state.go("details", {'bookId': bookId});
        };
        
        
        vm.getBooks = function () {
            console.log($stateParams.limit);
            console.log($stateParams.offset);
            dbService
                .bookList($stateParams.limit, $stateParams.offset)
                .then(function (results) {
                    console.log('Successfully Loaded 10 books. . . ');
                    vm.books = results;
                });
        };
        
        vm.getBooks();

    }

    detailCtrl.$inject = ['$state', '$stateParams', 'dbService', '$http'];

    function detailCtrl($state, $stateParams, dbService, $http) {
        var vm = this;
        vm.bookinfo = {};
        vm.bookinfo.id = "";
        vm.bookinfo.title = "";
        vm.bookinfo.author_lastname = "";
        vm.bookinfo.author_firstname = "";
        vm.status = {
            message: "",
            code: 0
        };
        
        
        vm.back = function () {
            $state.go("list");
        };

        dbService
            .bookDetail($stateParams.bookId)
            .then(function (bookinfo) {
                vm.bookinfo = bookinfo;
            });
        

        vm.edit = function (bookId) {
            

            vm.bookinfo.id = bookId;
            $http.post("/api/bookinfo/update/", vm.bookinfo)
                .then(function () {
                    vm.status.message = "The book is updated to the database.";
                    vm.status.code = 202;
                }).catch(function () {
                console.info("Error");
                vm.status.message = "Failed to update book to the database.";
                vm.status.code = 400;
            });
        };

    }


})();




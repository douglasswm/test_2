(function() {
    'use strict';

    angular
        .module('BookApp')
        .config(bookConfig);

    bookConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function bookConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("list", {
                url: "/list?limit&offset",
                templateUrl: "views/listView.html",
                controller: "listCtrl",
                controllerAs: "listCtrl"
            })
            .state("details", {
                url: "/details/:bookId",
                templateUrl: "views/bookView.html"
            });

        $urlRouterProvider.otherwise("/list");
    }
})();

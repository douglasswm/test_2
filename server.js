
var express = require("express");

var app = express();

var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var q = require('q');

var mysql = require("mysql");

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "123456",
    database: "booklibrary",
    connectionLimit: 4
});

const findAllBooks = "select id, author_lastname, author_firstname, title," +
    " cover_thumbnail from books ORDER BY title limit ? offset ?";
const findOneBook = "select * from books where id = ?";
const updateOneBook = "UPDATE books SET title = ?, author_lastname = ?, author_firstname =? where id = ?";


var makeQuery = function (sql, pool) {
    return (function (args) {
        var defer = q.defer();
        pool.getConnection(function (err, conn) {
            if (err) {
                
                defer.reject(err);
                return;
            }
            console.log(args);
            conn.query(sql, args || [], function (err, results) {
                conn.release();
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(results);
            });
        });
        return (defer.promise);
    });
};

var findAll = makeQuery(findAllBooks, pool);
var findOne = makeQuery(findOneBook, pool);
var updateOne = makeQuery(updateOneBook, pool);


app.use(express.static(__dirname + "/public")); // Serve files from public

app.get("/api/books", function (req, res) {
    var limit = parseInt(req.query.limit) || 125;
    var offset = parseInt(req.query.offset) || 0;
    findAll([limit, offset])
        .then(function (results) {
            res.status(200).json(results);
        });
});

app.get("/api/bookinfo/:bookId", function (req, res) {
    findOne([req.params.bookId])
        .then(function (results) {
            if (results.length) {
                res.status(200).json(results[0]);
            }
            else {
                res.status(404).end();
            }
        });
});

app.post("/api/bookinfo/update/", function (req, res) {
    console.info(req);
    updateOne([req.body.title, req.body.author_lastname, req.body.author_firstname,req.body.id])
        .then(function (results) {
            if (results) {
                res.status(200).json(results[0]);
            }
            else {
                // console.log(results.length);
                res.status(404).end();
            }
        });
});


app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});



